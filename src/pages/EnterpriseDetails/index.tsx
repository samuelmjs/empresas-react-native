import React, { useEffect, useState } from 'react';
import { StatusBar, Alert } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { useTheme } from 'styled-components';
import { Feather } from '@expo/vector-icons';

import { ApplicationStore } from '../../store';
import { Headers } from '../../store/ducks/auth/types';

import { EnterpriseProps } from '../../components/Enterprise';

import api from '../../services/api';

import {
  Container,
  Content,
  BackButton,
  Enterprise,
  Photo,
  EnterpriseWrapper,
  EnterpriseInfo,
  Description,
  Name,
  Price,
  Location,
  LocationContent,
  City,
  Separator,
  Country,
  EnterpriseDescription,
} from './styles';

interface RouteParams {
  id: number;
}

interface EnterpriseDataProps extends EnterpriseProps {
  email_enterprise: string;
  description: string;
  city: string;
  country: string;
}

function EnterpriseDetails() {
  const [enterprise, setEnterprise] = useState<EnterpriseDataProps>(
    {} as EnterpriseDataProps,
  );

  const headers = useSelector<ApplicationStore, Headers>(
    state => state.auth.headers,
  );

  const theme = useTheme();

  const navigation = useNavigation();
  const route = useRoute();
  const params = route.params as RouteParams;

  function handleBack() {
    navigation.goBack();
  }

  useEffect(() => {
    async function getEnterprisesDetails() {
      try {
        const response = await api.get(`enterprises/${params.id}`, {
          headers: {
            uid: headers.uid,
            client: headers.client,
            'access-token': headers.access_token,
          },
        });

        setEnterprise({
          ...response.data.enterprise,
          formattedSharePrice: new Intl.NumberFormat('pt-br', {
            style: 'currency',
            currency: 'BRL',
          }).format(response.data.enterprise.share_price),
        });
      } catch (err) {
        Alert.alert('Erro ao buscar dados', 'Tente mais tarde');
      }
    }

    getEnterprisesDetails();
  }, [
    enterprise.share_price,
    headers.access_token,
    headers.client,
    headers.uid,
    params.id,
  ]);

  return (
    <Container>
      <StatusBar barStyle="dark-content" />

      <BackButton onPress={handleBack}>
        <Feather name="chevron-left" size={25} color={theme.colors.primary} />
      </BackButton>

      <Content>
        <Enterprise>
          <Photo
            source={{
              uri: `https://empresas.ioasys.com.br/${enterprise.photo}`,
            }}
            resizeMode="cover"
          />

          <EnterpriseWrapper>
            <EnterpriseInfo>
              <Description>
                {enterprise.enterprise_type?.enterprise_type_name}
              </Description>
              <Name numberOfLines={2}>{enterprise.enterprise_name}</Name>
            </EnterpriseInfo>

            <EnterpriseInfo>
              <Description>Preço do dia</Description>
              <Price numberOfLines={1}>{enterprise.formattedSharePrice}</Price>
            </EnterpriseInfo>
          </EnterpriseWrapper>
        </Enterprise>

        <Location>
          <Feather name="map-pin" size={24} color={theme.colors.text_details} />

          <LocationContent>
            <City>{enterprise.city}</City>
            <Separator />
            <Country>{enterprise.country}</Country>
          </LocationContent>
        </Location>

        <EnterpriseDescription>{enterprise.description}</EnterpriseDescription>
      </Content>
    </Container>
  );
}

export { EnterpriseDetails };
