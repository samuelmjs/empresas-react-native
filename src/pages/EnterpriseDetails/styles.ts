import styled from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

export const Container = styled.View`
  background: ${({ theme }) => theme.colors.shapes};
  flex: 1;
`;

export const Content = styled.ScrollView.attrs({
  contentContainerStyle: {
    padding: 24,
  },
})``;

export const BackButton = styled(BorderlessButton)`
  padding: ${getStatusBarHeight() + 24}px 24px 24px;
`;

export const Enterprise = styled.View`
  border-radius: 5px;
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};

  padding: 16px;
  margin-bottom: 16px;

  flex-direction: row;
`;

export const Photo = styled.Image`
  height: ${RFValue(115)}px;
  width: ${RFValue(115)}px;

  border-radius: 5px;
  margin-right: 12px;
`;

export const EnterpriseWrapper = styled.View`
  justify-content: space-between;
`;

export const EnterpriseInfo = styled.View``;

export const Description = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(10)}px;
  color: ${({ theme }) => theme.colors.text_details};

  margin-bottom: 4px;

  text-transform: uppercase;
`;

export const Name = styled.Text`
  width: ${RFValue(180)}px;
  margin-bottom: 16px;
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.title};
`;

export const Price = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.success};
`;

export const Location = styled.View`
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};
  padding: 16px;
  margin-bottom: 24px;
  border-radius: 5px;

  flex-direction: row;
  align-items: center;
`;

export const LocationContent = styled.View`
  margin-left: 16px;

  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const City = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};
`;

export const Separator = styled.View`
  height: ${RFValue(6)}px;
  width: ${RFValue(6)}px;
  background-color: ${({ theme }) => theme.colors.text_details};

  margin: 0 8px;

  border-radius: 3px;
`;

export const Country = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};
`;

export const EnterpriseDescription = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};
  line-height: 24px;
`;
