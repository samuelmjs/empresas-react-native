import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme } from 'styled-components';
import { Feather } from '@expo/vector-icons';

import { ApplicationStore } from '../../store';
import { Investor } from '../../store/ducks/profile/types';
import { signOut } from '../../store/ducks/auth/actions';

import {
  Container,
  Header,
  Title,
  LogOut,
  AvatarContainer,
  Avatar,
  Name,
  Balance,
  EmailContent,
  Email,
  Location,
  LocationContent,
  City,
  Separator,
  Country,
} from './styles';

function Profile() {
  const investor = useSelector<ApplicationStore, Investor>(
    state => state.profile.investor,
  );
  const dispatch = useDispatch();
  const theme = useTheme();

  function handleLogOut() {
    dispatch(signOut());
  }

  return (
    <Container>
      <Header>
        <Title>Perfil</Title>

        <LogOut onPress={handleLogOut}>
          <Feather name="log-out" size={25} color={theme.colors.text_details} />
        </LogOut>
      </Header>

      <AvatarContainer>
        {investor.photo ? (
          <Avatar source={{ uri: investor.photo }} />
        ) : (
          <Feather name="user" size={80} color={theme.colors.primary} />
        )}
      </AvatarContainer>

      <Name>{investor.investor_name}</Name>
      <Balance>{investor.balanceFormatted}</Balance>

      <EmailContent>
        <Feather name="mail" size={24} color={theme.colors.text_details} />

        <Email>{investor.email}</Email>
      </EmailContent>

      <Location>
        <Feather name="map-pin" size={24} color={theme.colors.text_details} />

        <LocationContent>
          <City>{investor.city}</City>
          <Separator />
          <Country>{investor.country}</Country>
        </LocationContent>
      </Location>
    </Container>
  );
}

export { Profile };
