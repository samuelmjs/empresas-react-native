import styled from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.shapes};

  flex: 1;
`;

export const Header = styled.View`
  background-color: ${({ theme }) => theme.colors.primary};
  height: 240px;
  padding: ${getStatusBarHeight() + 24}px 24px 24px;

  flex-direction: row;
  justify-content: space-between;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.shapes};
`;

export const LogOut = styled(BorderlessButton)``;

export const AvatarContainer = styled.View`
  height: ${RFValue(190)}px;
  width: ${RFValue(190)}px;
  background-color: ${({ theme }) => theme.colors.shapes};
  border-radius: 100px;
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};
  margin-top: -95px;
  margin-bottom: 20px;

  align-self: center;
  align-items: center;
  justify-content: center;
`;

export const Avatar = styled.Image``;

export const Name = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  color: ${({ theme }) => theme.colors.title};
  font-size: ${RFValue(32)}px;
  margin-bottom: 8px;

  text-align: center;
`;

export const Balance = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.success};
  margin-bottom: 24px;

  text-align: center;
`;

export const EmailContent = styled.View`
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};
  padding: 16px;
  margin: 0 24px;
  margin-bottom: 8px;
  border-radius: 5px;

  flex-direction: row;
  align-items: center;
`;

export const Email = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};

  margin-left: 16px;
`;

export const Location = styled.View`
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};
  padding: 16px;
  margin: 0 24px;
  border-radius: 5px;

  flex-direction: row;
  align-items: center;
`;

export const LocationContent = styled.View`
  margin-left: 16px;

  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const City = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};
`;

export const Separator = styled.View`
  height: 6px;
  width: 6px;
  background-color: ${({ theme }) => theme.colors.text_details};

  margin: 0 8px;

  border-radius: 3px;
`;

export const Country = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.text};
`;
