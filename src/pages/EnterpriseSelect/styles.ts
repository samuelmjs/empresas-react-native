import styled from 'styled-components/native';
import {
  RectButton,
  GestureHandlerRootView,
} from 'react-native-gesture-handler';
import { FlatList } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

import { EnterpriseType } from './index';

interface EnterpriseSelectProps {
  isActive: boolean;
}

export const Container = styled(GestureHandlerRootView)`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.shapes};
`;

export const Header = styled.View`
  width: 100%;
  height: ${RFValue(113)}px;

  background-color: ${({ theme }) => theme.colors.primary};

  align-items: center;
  justify-content: flex-end;
  padding-bottom: 19px;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.shapes};
`;

export const EnterpriseTypesList = styled(
  FlatList as new () => FlatList<EnterpriseType>,
).attrs({
  contentContainerStyle: { paddingBottom: 24 },
})`
  flex: 1;
  width: 100%;
`;

export const EnterpriseSelectContainer = styled(
  RectButton,
)<EnterpriseSelectProps>`
  width: 100%;
  padding: 16px 24px;

  background-color: ${({ isActive, theme }) =>
    isActive ? theme.colors.background_secondary : theme.colors.shapes};
`;

export const EnterpriseSelectText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(17)}px;
  color: ${({ theme }) => theme.colors.title};
`;

export const Separator = styled.View`
  height: ${RFValue(1)}px;
  width: 100%;

  background-color: ${({ theme }) => theme.colors.background_secondary};
`;

export const Footer = styled.View`
  padding: 24px;
`;
