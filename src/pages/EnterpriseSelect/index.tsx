import React from 'react';

import { enterpriseTypes } from '../../utils/EnterpriseTypes';

import { Button } from '../../components/Button';

import {
  Container,
  Header,
  Title,
  EnterpriseTypesList,
  EnterpriseSelectContainer,
  EnterpriseSelectText,
  Separator,
  Footer,
} from './styles';

export interface EnterpriseType {
  id: number;
  name: string;
}

interface Props {
  enterpriseType?: number;
  setEnterpriseType: (id: number) => void;
  closeSelectEnterpriseType: () => void;
}

function EnterpriseSelect({
  enterpriseType,
  setEnterpriseType,
  closeSelectEnterpriseType,
}: Props) {
  function handleEnterpriseTypeSelect(id: number) {
    setEnterpriseType(id);
  }

  return (
    <Container>
      <Header>
        <Title>Filtros</Title>
      </Header>

      <EnterpriseTypesList
        data={enterpriseTypes}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => (
          <EnterpriseSelectContainer
            onPress={() => handleEnterpriseTypeSelect(item.id)}
            isActive={enterpriseType === item.id}
          >
            <EnterpriseSelectText>{item.name}</EnterpriseSelectText>
          </EnterpriseSelectContainer>
        )}
        ItemSeparatorComponent={() => <Separator />}
      />

      <Footer>
        <Button title="Aplicar" onPress={closeSelectEnterpriseType} />
      </Footer>
    </Container>
  );
}

export { EnterpriseSelect };
