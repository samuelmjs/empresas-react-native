import styled from 'styled-components/native';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled(GestureHandlerRootView)``;

export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ImageBackground = styled.Image``;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_700};
  font-size: ${RFValue(32)}px;
  color: ${({ theme }) => theme.colors.title};
  line-height: 40px;

  margin-left: 27px;
`;

export const Separator = styled.View`
  width: 100%;
  height: ${RFValue(6)}px;
  background-color: ${({ theme }) => theme.colors.primary};
`;

export const Form = styled.View`
  justify-content: center;
  padding: 40px;
`;

export const LoginText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_700};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.title};

  margin-bottom: 24px;
`;

export const Fields = styled.View`
  margin-bottom: 32px;
`;
