import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StatusBar, KeyboardAvoidingView } from 'react-native';
import * as Yup from 'yup';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import background from '../../assets/background.png';

import { Input } from '../../components/Input';
import { Button } from '../../components/Button';

import { signInRequest } from '../../store/ducks/auth/actions';
import { ApplicationStore } from '../../store';

import {
  Container,
  Header,
  ImageBackground,
  Title,
  Separator,
  Form,
  LoginText,
  Fields,
} from './styles';

interface FormData {
  email: string;
  password: string;
}

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Digite um e-mail válido')
    .required('Email é obrigatório'),
  password: Yup.string().required('Senha é obrigatória'),
});

function Login() {
  const dispatch = useDispatch();

  const loading = useSelector<ApplicationStore, boolean>(
    store => store.auth.loading,
  );

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  function handleSignIn({ email, password }: FormData) {
    dispatch(signInRequest({ email, password }));
  }

  return (
    <KeyboardAvoidingView behavior="position">
      <Container>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="transparent"
          translucent
        />
        <Header>
          <Title>
            Saiba mais {'\n'}
            sobre as {'\n'}
            empresas
          </Title>
          <ImageBackground source={background} />
        </Header>
        <Separator />

        <Form>
          <LoginText>Faça seu Login</LoginText>

          <Fields>
            <Input
              name="email"
              control={control}
              error={errors.email && errors.email.message}
              title="E-mail"
              keyboardType="email-address"
              placeholder="Ex: exemplo@gmail.com"
              autoCorrect={false}
              autoCapitalize="none"
            />
            <Input
              name="password"
              control={control}
              error={errors.password && errors.password.message}
              title="Password"
              placeholder="Sua senha secreta"
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="send"
              onSubmitEditing={handleSubmit(handleSignIn)}
            />
          </Fields>

          <Button
            title="Entrar"
            loading={loading}
            onPress={handleSubmit(handleSignIn)}
          />
        </Form>
      </Container>
    </KeyboardAvoidingView>
  );
}

export { Login };
