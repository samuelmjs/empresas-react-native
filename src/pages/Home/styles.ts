import styled from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { FlatList } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

import { EnterpriseProps } from '../../components/Enterprise';

export const Container = styled.View`
  position: relative;
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background_primary};
`;

export const FilterContainer = styled(BorderlessButton)`
  position: absolute;
  width: ${RFValue(56)}px;
  height: ${RFValue(56)}px;
  background-color: ${({ theme }) => theme.colors.shapes};
  z-index: 5;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);

  border-radius: 32px;

  justify-content: center;
  align-items: center;

  bottom: 24px;
  right: 24px;
`;

export const Filter = styled.View`
  position: absolute;
  z-index: 10;
  right: 5px;
  top: 0;
  height: 12px;
  width: 12px;
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 6px;
`;

export const Header = styled.View`
  height: ${RFValue(161)}px;
  background-color: ${({ theme }) => theme.colors.primary};

  padding: ${getStatusBarHeight() + 24}px 16px;

  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(24)}px;
  color: ${({ theme }) => theme.colors.shapes};
`;

export const TotalEnterprises = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text};
`;

export const SearchContainer = styled.View`
  background-color: ${({ theme }) => theme.colors.shapes};
  border-radius: 5px;
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};

  padding: 0 16px;
  margin: 0 16px;

  margin-top: -30px;

  flex-direction: row;
  align-items: center;
`;

export const Search = styled.TextInput`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.primary};

  padding: 18px 12px;

  flex: 1;
`;

export const CloseSearchButton = styled.TouchableOpacity`
  height: ${RFValue(56)}px;

  align-items: center;
  justify-content: center;
`;

export const CloseSearchText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(12)}px;
  color: ${({ theme }) => theme.colors.text};
`;

export const EnterpriseList = styled(
  FlatList as new () => FlatList<EnterpriseProps>,
).attrs({
  contentContainerStyle: { padding: 16 },
  showsVerticalScrollIndicator: false,
})``;
