import React, { useState, useEffect, useCallback } from 'react';
import { Alert, Modal, Keyboard } from 'react-native';
import { useSelector } from 'react-redux';
import { useTheme } from 'styled-components';
import { Feather } from '@expo/vector-icons';

import api from '../../services/api';
import { ApplicationStore } from '../../store';
import { Headers } from '../../store/ducks/auth/types';
import { Enterprise, EnterpriseProps } from '../../components/Enterprise';
import { EnterpriseSelect } from '../EnterpriseSelect';

import {
  Container,
  FilterContainer,
  Filter,
  Header,
  Title,
  TotalEnterprises,
  SearchContainer,
  Search,
  CloseSearchButton,
  CloseSearchText,
  EnterpriseList,
} from './styles';

interface SearchProps {
  name?: string;
  enterprise_types?: number;
}

function Home() {
  const [search, setSearch] = useState<SearchProps>({} as SearchProps);
  const [enterprises, setEnterprises] = useState<EnterpriseProps[]>([]);
  const [enterpriseTypeModalOpen, setEnterpriseTypeModalOpen] = useState(false);

  const headers = useSelector<ApplicationStore, Headers>(
    state => state.auth.headers,
  );

  const theme = useTheme();

  function handleOpenSelectEnterpriseTypeModal() {
    setEnterpriseTypeModalOpen(true);
  }

  function handleCloseSelectEnterpriseTypeModal() {
    setEnterpriseTypeModalOpen(false);
  }

  function handleSelectedEnterpriseType(id: number) {
    if (id === search.enterprise_types) {
      setSearch({ ...search, enterprise_types: undefined });

      return;
    }

    setSearch({ ...search, enterprise_types: id });
  }

  function handleCancelSearch() {
    Keyboard.dismiss();
    setSearch({ name: undefined, enterprise_types: undefined });
  }

  const getEnterprises = useCallback(
    async ({ name, enterprise_types }: SearchProps) => {
      try {
        const response = await api.get('enterprises', {
          params: {
            name,
            enterprise_types,
          },
          headers: {
            uid: headers.uid,
            client: headers.client,
            'access-token': headers.access_token,
          },
        });

        setEnterprises(
          response.data.enterprises.map((enterprise: EnterpriseProps) => ({
            ...enterprise,
            formattedSharePrice: new Intl.NumberFormat('pt-br', {
              style: 'currency',
              currency: 'BRL',
            }).format(enterprise.share_price),
          })),
        );
      } catch (err) {
        Alert.alert('Erro ao buscar dados', 'Tente mais tarde');
      }
    },
    [headers.access_token, headers.client, headers.uid],
  );

  useEffect(() => {
    getEnterprises({ ...search });
  }, [getEnterprises, search]);

  return (
    <Container>
      <FilterContainer onPress={handleOpenSelectEnterpriseTypeModal}>
        {!!search.enterprise_types && <Filter />}

        <Feather name="filter" size={24} color={theme.colors.primary} />
      </FilterContainer>

      <Header>
        <Title>Listagem</Title>

        <TotalEnterprises>{enterprises.length} empresas</TotalEnterprises>
      </Header>

      <SearchContainer>
        <Feather name="search" size={24} color={theme.colors.text} />

        <Search
          autoCapitalize="none"
          autoCorrect={false}
          placeholder="Busque uma empresa"
          value={search.name}
          onChangeText={text => setSearch({ ...search, name: text })}
          returnKeyType="send"
        />

        {!!search.name && (
          <CloseSearchButton onPress={handleCancelSearch}>
            <CloseSearchText>Cancelar</CloseSearchText>
          </CloseSearchButton>
        )}
      </SearchContainer>

      <EnterpriseList
        data={enterprises}
        keyExtractor={enterprise => String(enterprise.id)}
        renderItem={({ item }) => <Enterprise data={item} />}
      />

      <Modal visible={enterpriseTypeModalOpen}>
        <EnterpriseSelect
          enterpriseType={search.enterprise_types}
          setEnterpriseType={id => handleSelectedEnterpriseType(id)}
          closeSelectEnterpriseType={handleCloseSelectEnterpriseTypeModal}
        />
      </Modal>
    </Container>
  );
}

export { Home };
