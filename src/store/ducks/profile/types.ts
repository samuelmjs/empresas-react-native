export interface Investor {
  id?: number;
  investor_name?: string;
  email?: string;
  city?: string;
  country?: string;
  balance?: number;
  balanceFormatted?: number;
  photo?: string;
  portfolio_value?: number;
  first_access?: boolean;
  super_angel?: boolean;
}

export interface ProfileState {
  readonly investor: Investor;
}
