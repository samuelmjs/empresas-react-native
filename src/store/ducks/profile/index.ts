import { Reducer } from 'redux';

import { AuthTypes } from '../auth/types';
import { ProfileState } from './types';

const INITIAL_STATE: ProfileState = {
  investor: {},
};

const reducer: Reducer<ProfileState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AuthTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        investor: {
          ...action.payload.investor,
          balanceFormatted: new Intl.NumberFormat('pt-br', {
            style: 'currency',
            currency: 'BRL',
          }).format(action.payload.investor.balance),
        },
      };

    case AuthTypes.SIGN_OUT:
      return { ...state, investor: {} };

    default:
      return state;
  }
};

export default reducer;
