import { AuthTypes, Headers } from './types';
import { Investor } from '../profile/types';

interface Request {
  email: string;
  password: string;
}

export function signInRequest({ email, password }: Request) {
  return {
    type: AuthTypes.SIGN_IN_REQUEST,
    payload: { email, password },
  };
}

export function signInSuccess(investor: Investor, headers: Headers) {
  return {
    type: AuthTypes.SIGN_IN_SUCCESS,
    payload: {
      investor,
      headers,
    },
  };
}

export function signInFailure() {
  return {
    type: AuthTypes.SIGN_IN_FAILURE,
  };
}

export function signOut() {
  return {
    type: AuthTypes.SIGN_OUT,
  };
}
