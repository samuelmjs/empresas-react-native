import { Reducer } from 'redux';

import { AuthState, AuthTypes } from './types';

const INITIAL_STATE: AuthState = {
  headers: {},
  loading: false,
};

const reducer: Reducer<AuthState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AuthTypes.SIGN_IN_REQUEST:
      return { ...state, loading: true };

    case AuthTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        headers: {
          client: action.payload.headers.client,
          uid: action.payload.headers.uid,
          access_token: action.payload.headers['access-token'],
        },
        loading: false,
      };

    case AuthTypes.SIGN_IN_FAILURE:
      return { ...state, loading: false };

    case AuthTypes.SIGN_OUT:
      return {
        ...state,
        header: {},
        loading: false,
      };

    default:
      return state;
  }
};

export default reducer;
