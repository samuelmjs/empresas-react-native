import { Alert } from 'react-native';
import { call, put } from 'redux-saga/effects';
import { AxiosResponse } from 'axios';

import api from '../../../services/api';

import { Investor } from '../profile/types';
import { signInFailure, signInSuccess } from './actions';

interface SignProps {
  payload: {
    email: string;
    password: string;
  };
}

interface Response {
  investor: Investor;
}

export function* signIn({ payload }: SignProps) {
  try {
    const response: AxiosResponse<Response> = yield call(
      api.post,
      'users/auth/sign_in',
      payload,
    );

    api.defaults.headers.uid = response.headers.uid;
    api.defaults.headers.client = response.headers.client;
    api.defaults.headers['access-token'] = response.headers['access-token'];

    yield put(signInSuccess(response.data.investor, response.headers));
  } catch {
    Alert.alert(
      'Falha na autenticação',
      'Houve um erro no login, verifique seus dados',
    );

    yield put(signInFailure());
  }
}
