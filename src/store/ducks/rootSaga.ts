import { takeLatest, all } from 'redux-saga/effects';

import { AuthTypes } from './auth/types';
import { signIn } from './auth/sagas';

export default function* rootSaga(): any {
  return yield all([takeLatest<any>(AuthTypes.SIGN_IN_REQUEST, signIn)]);
}
