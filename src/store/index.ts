import { createStore, Store, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { AuthState } from './ducks/auth/types';
import { ProfileState } from './ducks/profile/types';

import rootReducer from './ducks/rootReducer';
import rootSaga from './ducks/rootSaga';

export interface ApplicationStore {
  auth: AuthState;
  profile: ProfileState;
}

const sagaMiddleware = createSagaMiddleware();

const store: Store<ApplicationStore> = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);

export default store;
