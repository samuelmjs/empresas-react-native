export default {
  colors: {
    primary: '#1B1B1F',
    primary_light: '##DEDEE3;',

    title: '#47474D',
    text: '#7A7A80',
    text_details: '#AEAEB3',

    success: '#03B352',

    error: '#DC1637',

    background_primary: '#F4F5F6',
    background_secondary: '#EBEBF0',

    shapes: '#fff',
  },

  fonts: {
    primary_400: 'Roboto_400Regular',
    primary_500: 'Roboto_500Medium',
    primary_700: 'Roboto_700Bold',
  },
};
