import styled from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled(BorderlessButton)`
  background-color: ${({ theme }) => theme.colors.shapes};
  border-radius: 5px;
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};

  padding: 16px;
  margin-bottom: 8px;

  flex-direction: row;
`;

export const PhotoContainer = styled.View`
  height: ${RFValue(86)}px;
  width: ${RFValue(86)}px;

  border-radius: 5px;
  margin-right: 12px;

  justify-content: center;
  align-items: center;
`;

export const Photo = styled.Image`
  height: 86px;
  width: 86px;
  border-radius: 5px;
`;

export const EnterpriseWrapper = styled.View`
  justify-content: space-between;
`;

export const EnterpriseInfo = styled.View``;

export const Description = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(10)}px;
  color: ${({ theme }) => theme.colors.text_details};

  margin-bottom: 4px;

  text-transform: uppercase;
`;

export const Name = styled.Text`
  max-width: ${RFValue(200)}px;
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.title};
`;

export const Price = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_500};
  font-size: ${RFValue(16)}px;
  color: ${({ theme }) => theme.colors.success};
`;
