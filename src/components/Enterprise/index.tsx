import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useTheme } from 'styled-components';
import { Feather } from '@expo/vector-icons';

import {
  Container,
  EnterpriseWrapper,
  Description,
  PhotoContainer,
  Photo,
  Name,
  Price,
  EnterpriseInfo,
} from './styles';

type RootStackParamList = {
  EnterpriseDetails: { id: number };
};

type EnterpriseDetailsScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'EnterpriseDetails'
>;

export interface EnterpriseProps {
  id: number;
  enterprise_name: string;
  photo: string;
  share_price: number;
  formattedSharePrice: number;
  enterprise_type: {
    id: number;
    enterprise_type_name: string;
  };
}

interface Props {
  data: EnterpriseProps;
}

function Enterprise({ data }: Props) {
  const theme = useTheme();
  const navigation = useNavigation<EnterpriseDetailsScreenNavigationProp>();

  function handleNavigateToEnterpriseDetails(id: number) {
    navigation.navigate('EnterpriseDetails', {
      id,
    });
  }

  return (
    <Container
      onPress={() => {
        handleNavigateToEnterpriseDetails(data.id);
      }}
    >
      <PhotoContainer>
        {data.photo ? (
          <Photo
            source={{
              uri: `https://empresas.ioasys.com.br/${data.photo}`,
            }}
            resizeMode="cover"
          />
        ) : (
          <Feather name="image" size={32} color={theme.colors.text_details} />
        )}
      </PhotoContainer>

      <EnterpriseWrapper>
        <EnterpriseInfo>
          <Description>{data.enterprise_type.enterprise_type_name}</Description>
          <Name numberOfLines={1}>{data.enterprise_name}</Name>
        </EnterpriseInfo>

        <EnterpriseInfo>
          <Description>Preço do dia</Description>
          <Price>{data.formattedSharePrice}</Price>
        </EnterpriseInfo>
      </EnterpriseWrapper>
    </Container>
  );
}

export { Enterprise };
