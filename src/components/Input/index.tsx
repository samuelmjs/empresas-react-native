import React, { useState } from 'react';
import { TextInputProps } from 'react-native';
import { useTheme } from 'styled-components';
import { Control, Controller } from 'react-hook-form';
import { Feather } from '@expo/vector-icons';

import {
  Container,
  FormInput,
  Label,
  Error,
  InputContainer,
  VisiblePasswordButton,
} from './styles';

interface Props extends TextInputProps {
  control: Control;
  title: string;
  name: string;
  error: string;
}

function Input({ control, title, name, error, ...rest }: Props) {
  const [focused, setFocused] = useState(false);
  const [isVisiblePassword, setIsVisiblePassword] = useState(false);

  const theme = useTheme();

  function handleFocus(isFocused: boolean) {
    setFocused(isFocused);
  }

  function handleVisiblePassword() {
    setIsVisiblePassword(oldVisible => !oldVisible);
  }

  return (
    <Container>
      <Label>{title}</Label>
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value } }) => (
          <InputContainer focused={focused} error={!!error}>
            <FormInput
              {...rest}
              onFocus={() => handleFocus(true)}
              onBlur={() => handleFocus(false)}
              onChangeText={onChange}
              value={value}
              secureTextEntry={name === 'password' ? !isVisiblePassword : false}
            />
            {name === 'password' && value ? (
              <VisiblePasswordButton onPress={handleVisiblePassword}>
                <Feather
                  name={isVisiblePassword ? 'eye' : 'eye-off'}
                  size={24}
                  color={theme.colors.primary}
                />
              </VisiblePasswordButton>
            ) : null}
          </InputContainer>
        )}
      />
      {error && <Error>{error}</Error>}
    </Container>
  );
}

export { Input };
