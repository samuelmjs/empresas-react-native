import styled, { css } from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { TextInput } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

interface InputContainerProps {
  focused: boolean;
  error: boolean;
}

export const Container = styled.View`
  margin-bottom: 16px;
`;

export const FormInput = styled(TextInput)`
  font-size: ${RFValue(16)}px;
  height: 100%;
  padding-right: 16px;

  flex: 1;
`;

export const Label = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text};

  margin-bottom: 8px;
`;

export const Error = styled.Text`
  font-family: ${({ theme }) => theme.fonts.primary_400};
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.error};

  margin-top: 4px;
`;

export const InputContainer = styled.View<InputContainerProps>`
  height: 56px;
  background-color: ${({ theme }) => theme.colors.background_primary};
  border-radius: 5px;
  border: 1px solid ${({ theme }) => theme.colors.background_secondary};
  padding: 0px 16px;

  flex-direction: row;
  align-items: center;

  ${({ theme, focused }) =>
    focused &&
    css`
      border-color: ${theme.colors.primary};
    `}

  ${({ theme, error }) =>
    error &&
    css`
      border-color: ${theme.colors.error};
    `}
`;

export const VisiblePasswordButton = styled(BorderlessButton)`
  height: 100%;

  justify-content: center;
`;
