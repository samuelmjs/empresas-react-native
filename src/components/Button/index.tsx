import React from 'react';
import { useTheme } from 'styled-components';
import { TouchableOpacityProps } from 'react-native';

import { Container, ButtonText, Loading } from './styles';

interface Props extends TouchableOpacityProps {
  title: string;
  loading?: boolean;
}

function Button({ title, loading, ...rest }: Props) {
  const theme = useTheme();

  return (
    <Container {...rest}>
      {loading ? (
        <Loading size="small" color={theme.colors.background_secondary} />
      ) : (
        <ButtonText>{title}</ButtonText>
      )}
    </Container>
  );
}

export { Button };
