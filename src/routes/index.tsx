import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';

import { AuthRoutes } from './auth.routes';
import { AppRoutes } from './app.routes';

import { ApplicationStore } from '../store';

function Routes() {
  const signed = useSelector<ApplicationStore, number | undefined>(
    state => state.profile.investor.id,
  );

  return (
    <NavigationContainer>
      {signed ? <AppRoutes /> : <AuthRoutes />}
    </NavigationContainer>
  );
}

export { Routes };
