![N|Solid](logo_ioasys.png)


## Dados Gerais
  * Aplicação feita utilizando React Native com Expo
  * Funcinalidades Feitas (Login, Listagem de Empresas, Detalhes da empresa, Filtro por nome e tipo ).

## Justificativa das bibliotecas

### Dependências
   * `@expo-google-fonts/roboto` Para aplicar fonte personalizada
   * `@react-navigation/bottom-tabs` Para navegação por meio de tabs(Tela home e Perfil)
   * `@react-navigation/native` Para ser aplicado navegação no React
   * `@react-navigation/stack` Para navegação por meio de stack (empresas -> detallhe )
   * `axios` Acessar api do projeto
   * `eslint-config-airbnb` Padronização de código
   * `expo-app-loading` Tratar carregamento de fonte personalizada
   * `expo-font` Para aplicar fonte personalizada
   * `react-native-gesture-handler` Utilizado para trazer experiência nativa nos botões
   * `react-native-iphone-x-helper` Tratar peculiaridades do Iphone
   * `react-native-responsive-fontsize` Responsividade de fontes e elementos 
   * `react-redux` Gerenciamento de estado
   * `redux` Gerenciamento de estado
   * `redux-saga` Middleware para redux
   * `styled-components` Estilização
   * `yup` Validação de formulário

### Dependências de Desenvolvimento
  * `@types/react` Tipagem 
  * `@types/react-native` Tipagem
  * `@types/react-redux` Tipagem,
  * `@types/styled-components-react-native` Padronização de código,
  * `@typescript-eslint/eslint-plugin` Padronização de código,
  * `@typescript-eslint/parser` Padronização de código,
  * `eslint` Padronização de código,
  * `eslint-config-prettier` Padronização de código,
  * `eslint-import-resolver-typescript` Tipagem,
  * `eslint-plugin-import` Padronização de código,
  * `eslint-plugin-jsx-a11y` Padronização de código,
  * `eslint-plugin-prettier` Padronização de código,
  * `eslint-plugin-react` Padronização de código,
  * `eslint-plugin-react-hooks` Padronização de código,
  * `prettier` Padronização de código,
  * `typescript` Tipagem

### Como rodar o projeto
  Obs: Está aplicação foi testada somente em IOS

  * Baixe o app Expo na loja do seu celular

  * Rode npm install --global expo-cli

  * Rode yarn na raiz do projeto para instalar as dependências

  * Rode yarn start para iniciar o client web

  * Aponte a câmera para o QRCode que irá paracer e aguarde a inicialização
  
---

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

---

### Objetivo ###

* Desenvolver uma aplicação React Native que consuma a API `Empresas`, cujo Postman esta compartilhado neste repositório (collection).
* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um Pull Request para isso.
* Nós iremos realizar a avaliação e te retornar um email com o resultado.


### O que será avaliado?
* A ideia com este teste é ter um melhor entendimento das suas habilidades com Javascript e React Native. Mas de qualquer forma, um layout bonito e com boa usabilidade é **MUITO** bem vindo.
- A qualidade e desempenho do seu código
- Sua capacidade de organizar o código
- Capacidade de tomar decisões


### Escopo do Projeto
* O Login e acesso de Usuário já registrados
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers:
		* `access-token`;
		* `client`;
		* `uid`;
	* Para ter acesso às demais APIs precisamos enviar esses 3 (três) custom headers para a API autorizar a requisição;
* Endpoints disponíveis:
	* Listagem de Empresas: `/enterprises`
	* Detalhamento de Empresas: `/enterprises/{id}`
	* Filtro de Empresas por nome e tipo: `/enterprises?enterprise_types={type}&name={name}`
* Gostaríamos que todos os três endpoints disponibilizados fossem utilizados.

### Dados para Teste ###
* Servidor: http://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Informações Importantes
* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório. Para utilizar a collection, vá até o postman e importe a colllection que está disponível neste repositório. Assim você terá acesso às documentação da API.
* É obrigatório utilização do React Native
* A sua aplicação deve possuir mais de uma tela. Entretanto, a disposição do conteúdo entre elas fica ao seu critério.
* O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.
* O `README.md` do projeto deve conter instruções de como executar a aplicação
* Independente de onde conseguiu chegar no teste, é importante compartilhar o ponto em que você parou para analisarmos.

### Dicas
* No Postman existem alguns parâmetros no header que devem ser passados em todas requests exceto na de login, eles serão retornados no endpoint de login, nos headers da request.
* Evite utilizar muitas bibliotecas que não sejam diretamente relacionadas ao build da aplicação. O uso das mesmas não esta vetado, mas seria interessante ver como você faz seus componentes :)

### Bônus
* Utilização de Redux / Redux Saga.
* Utilização de linters ou outras ferramentas de análise estática
* Testes unitários, interface, etc.

### Sobrou tempo?
Aqui na **ioasys** nós prezamos muito pela autonomia e contribuição dos nossos funcionários. Então, caso tenha sobrado tempo, sinta-se livre para sugerir alguma melhoria neste desafio :)

